"use strict";

var axios = require("axios")["default"];

var cypress = require("cypress");

var path = require("path");

var express = require("express");

var schedule = require("node-schedule");

var server = express();
server.use(express.json());
server.use(express.urlencoded());
server.get("/", function _callee(req, res) {
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          res.send("D ja,");

        case 1:
        case "end":
          return _context.stop();
      }
    }
  });
});
server.get("/insert_bill", function _callee2(req, res) {
  var peak_url, check, test_result;
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          peak_url = "https://memorize-api-teulcdz4qa-as.a.run.app/peak/auto";
          _context2.next = 3;
          return regeneratorRuntime.awrap(axios.get(peak_url));

        case 3:
          check = _context2.sent;
          _context2.prev = 4;

          if (!(check.data.length > 0)) {
            _context2.next = 12;
            break;
          }

          console.log("running auto insert...");
          _context2.next = 9;
          return regeneratorRuntime.awrap(cypress.run({
            spec: path.join(__dirname, "./cypress/integration/realpeak.spec.js")
          }).then(function (result) {
            console.log(result);
            res.json({
              result: result
            });
          }));

        case 9:
          test_result = _context2.sent;
          _context2.next = 13;
          break;

        case 12:
          res.json({
            message: "data is empty."
          });

        case 13:
          _context2.next = 18;
          break;

        case 15:
          _context2.prev = 15;
          _context2.t0 = _context2["catch"](4);
          console.error(_context2.t0);

        case 18:
        case "end":
          return _context2.stop();
      }
    }
  }, null, null, [[4, 15]]);
});
console.log("Port : ", process.env.PORT);
server.listen(process.env.PORT || 8080, "0.0.0.0", function () {
  console.log("Start server.");
});
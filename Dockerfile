FROM node:14.16.0-buster-slim

RUN  apt-get update \
     && apt-get install -y wget gnupg ca-certificates procps libxss1 \
     && wget --quiet https://raw.githubusercontent.com/vishnubob/wait-for-it/master/wait-for-it.sh -O /usr/sbin/wait-for-it.sh \
     && chmod +x /usr/sbin/wait-for-it.sh \
     && apt-get install -y libgtk2.0-0 libgtk-3-0 libgbm-dev libnotify-dev libgconf-2-4 libnss3 libxss1 libasound2 libxtst6 xauth xvfb

WORKDIR /app

# Install Puppeteer under /node_modules so it's available system-wide
ADD package.json /app/
ADD app.js /app/
COPY cypress /app/cypress
ADD cypress.json /app/

RUN yarn

ENV PORT 8080
EXPOSE 8080

CMD ["node", "app.js" ]   



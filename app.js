const axios = require("axios").default;
const cypress = require("cypress");
const path = require("path");
const express = require("express");
const schedule = require("node-schedule");

const server = express();

server.use(express.json());
server.use(express.urlencoded());

server.get("/", async (req, res) => {
  res.send("D ja,");
});
server.get("/insert_bill", async (req, res) => {
  const peak_url = "https://memorize-api-teulcdz4qa-as.a.run.app/peak/auto";
  const check = await axios.get(peak_url);
  try {
    if (check.data.length > 0) {
      console.log("running auto insert...");
      const test_result = await cypress.run({
        spec: path.join(__dirname, "./cypress/integration/realpeak.spec.js"),
      }).then((result)=>{
        console.log(result);
        res.json({result});
      })
      // res.json({ test_result });
      // if (test_result.totalFailed == 0) {
      //   let message = `[สำเร็จ]\n${test_result.status}\n[version:10-09-2021]`;
      //   axios.post(
      //     "https://notify-api.line.me/api/notify",
      //     `message=${message}`,
      //     {
      //       headers: {
      //         Authorization: `Bearer ebKB3WVmJOGTPUhCeXSNK3cfrQZL0OblOq04mG33eEz`,
      //         "Content-Type": "application/x-www-form-urlencoded",
      //       },
      //     }
      //   );
      // } else {
      //   let message = `[ไม่สำเร็จ]\n${test_result.status}\n[version:10-9-2021]`;
      //   axios.post(
      //     "https://notify-api.line.me/api/notify",
      //     `message=${message}`,
      //     {
      //       headers: {
      //         Authorization: `Bearer ebKB3WVmJOGTPUhCeXSNK3cfrQZL0OblOq04mG33eEz`,
      //         "Content-Type": "application/x-www-form-urlencoded",
      //       },
      //     }
      //   );
      // }
    } else {
      res.json({ message: "data is empty." });
    }
  } catch (error) {
    console.error(error);
  }
});

console.log("Port : ", process.env.PORT);

server.listen(process.env.PORT || 8080, "0.0.0.0", () => {
  console.log("Start server.");
});

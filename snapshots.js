module.exports = {
  "__version": "9.0.0",
  "API Snapshot Testing": {
    "snapshots response": {
      "1": "Using fixtures to represent data",
      "2": "hello@cypress.io",
      "3": "Fixtures are a great way to mock data for responses to routes"
    }
  },
  "Snapshot Testing": {
    "snapshot DOM element": {
      "test-title": "<title cy-data=\"test-title\">Snapshot Testing By Golf</title>",
      "test-h2": "<h2 cy-data=\"test-h2\">Hello World</h2>",
      "test-body": "<p cy-data=\"test-body\">How to Snapshot Testing</p>"
    }
  },
  "Snapshot": {
    "snapshot": {
      "test-title": "<title data-cy=\"title\">Snapshot</title>",
      "test-h1": "<h1 data-cy=\"h1\">Snapshot with cypress</h1>",
      "test-p": "<p data-cy=\"p\">Hello</p>"
    }
  }
}

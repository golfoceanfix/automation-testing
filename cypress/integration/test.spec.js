describe('login', function () {
  beforeEach(function () {
    cy.visit('https://secure.peakaccount.com/');
    cy.Username().type('finance@memorizebrownie.com');
    cy.wait(200);
    cy.Password().type('memorize2020');
    cy.wait(600); // cy.get('.input').contains('อีเมล').type('sitthiphong.pro@gmail.com')
    // cy.get('.input').contains('รหัสผ่าน').type('Prompalit1')

    cy.get('button').click();
  });
  context('เลือกกิจการ', function () {
    it('ทดสอบ', function () {
      cy.request('GET', 'https://memorize-api-teulcdz4qa-as.a.run.app/peak/auto').then(function (data) {
        var item = data.body[0];
        cy.wait(1000);
        console.log(item); // cy.get('.lists > :nth-child(2)').click()

        for (var i = 0; i < data.body.length; i++) {
          cy.visit('https://secure.peakaccount.com/income/receiptCreate?emi=MTA1NzE5'); // วันที่ออก
          cy.wait(2000)
          if (item) {
            // cy.get('#CalendarSingle').click().type(`${item.issuedDate}{enter}`)
            cy.wait(1000);
            cy.get(':nth-child(3) > #CalendarSingle > [data-v-54d56a99=""][data-v-1bd0f1ef=""] > .input > input').click().type("".concat(item.issuedDate, "{enter}")); // ชื่อลูกค้า

            cy.get('#inputDropdown').click().type("".concat(item.peakContactCode));
            cy.get('[data-v-3ae4715c=""] > #inputDropdownDataList > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__content-wrapper').contains("".concat(item.peakContactName)).click(); // ปรเภทภาษี

            cy.get('#inputDropdownPricingType > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__tags').click();
            cy.get('#inputDropdownPricingType > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__content-wrapper').contains('รวมภาษี').click(); // ออกใบกำกับภาษี

            cy.get('.slider');
            var sum = 0;
            item.brachProduct.forEach(function (brachProduct) {
              return sum += brachProduct.peakProductPrice * brachProduct.peakProductQuantity;
            });
            console.log(sum);
            console.log(item.brachProduct);
            item.brachProduct.forEach(function (element, index) {
              // รายการสินค้า
              cy.get('#inputDropdownDataList > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__tags > .multiselect__placeholder').type("".concat(item.brachProduct[index].peakProductCode, "{enter}")); // บีญชี
              // จำนวน

              cy.get('.quantity').eq([index]).type("".concat(item.brachProduct[index].peakProductQuantity, "{enter}")); // ส่วนลด

              var disCount = item.brachProduct[index].peakProductDiscount / item.brachProduct[index].peakProductQuantity;
              cy.get('#inputDiscount > #inputComponent').eq([index]).type("".concat(disCount, "{enter}"));
              cy.get('#dropdownTax > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__tags > .multiselect__single > .singleLabel > p').eq([index]).click();
              cy.get('#dropdownTax > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__content-wrapper > .multiselect__content > :nth-child(3) > .multiselect__option').eq([index]).contains("7%").click({
                force: true
              });
              cy.get(':nth-child(1) > .secondary > .blue > button').click({
                force: true
              });
              cy.get('#dropdownAccoutId');
            });

            if (item.totalShippingFee != null) {
              cy.document().then(function (document) {
                var table = document.querySelectorAll('table.product');
                var account = table[table.length - 1].querySelectorAll('#dropdownAccoutId');
                cy.wrap(account[0]).as('ac');
                cy.get('@ac').type('420109{enter}');
                var price = table[table.length - 1].querySelectorAll('#inputComponent input[id]');
                cy.wrap(price[1]).as('item');
                cy.get('@item').type(item.totalShippingFee);
              });
            }

            if (item.manualDiscount != null) {
              cy.get('#checkbox').click();
              cy.get('.inputDiscount').type("".concat(item.manualDiscount));
            }

            cy.get('#DropdownPaymentBankAccount').type('เงินสด');
            cy.get('#DropdownPaymentBankAccount > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__tags').click();
            cy.get('#DropdownPaymentBankAccount > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__content-wrapper').contains('เงินสด').click();
            cy.wait(500);
            cy.intercept('POST', 'https://peakrealtimeservice-production.azurewebsites.net/api/Receipt/CreateReceiptOnce').as('CreateBill');
            cy.get('#FooterButton > :nth-child(1) > .end > :nth-child(3) > :nth-child(1) > .button > .buttonDefault > div > p').click({
              force: true
            }); // หัก ณ ที่จ่าย

            cy.wait('@CreateBill');
            cy.get('@CreateBill').then(function (res) {
              if (res.response.body.responseCode = 200) {
                cy.request('PATCH', "https://memorize-api-teulcdz4qa-as.a.run.app/peak/brachprocess/".concat(data.body[0].id, "/auto-fill"));
                var message = `[สร้างใบเสร็จสำเร็จ]\nเลขที่ : ${data.body[0].id}`
                cy.request({
                  method: 'POST',
                  url: 'https://notify-api.line.me/api/notify',
                  body: {
                    message: message
                  },
                  headers: {
                    Authorization: "Bearer ebKB3WVmJOGTPUhCeXSNK3cfrQZL0OblOq04mG33eEz",
                    "Content-Type": "application/x-www-form-urlencoded"
                  }
                });
              } else {
                var message = `[สร้างใบเสร็จล้มเหลว]\nเลขที่ : ${data.body[0].id}`

                cy.request({
                  method: 'POST',
                  url: 'https://notify-api.line.me/api/notify',
                  body: {
                    message: message
                  },
                  headers: {
                    Authorization: "Bearer ebKB3WVmJOGTPUhCeXSNK3cfrQZL0OblOq04mG33eEz",
                    "Content-Type": "application/x-www-form-urlencoded"
                  }
                });
              }
            });
          }
        }
      });
    });
  });
});
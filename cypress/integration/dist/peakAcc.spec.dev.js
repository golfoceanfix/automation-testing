"use strict";

describe("login", function () {
  beforeEach(function () {
    cy.login();
  });
  context("เลือกกิจการ", function () {
    it("ทดสอบ", function () {
      cy.request("GET", "https://memorize-api-teulcdz4qa-as.a.run.app/peak/auto").then(function (data) {
        var item = data.body[0];
        cy.wait(1000);
        console.log(item);
        cy.visit("https://secure.peakaccount.com/income/receiptCreate?emi=MTA1NzE5"); // วันที่ออก

        if (item) {
          cy.get("#CalendarSingle").click().type("".concat(item.issuedDate, "{enter}")); // ชื่อลูกค้า

          cy.get("#inputDropdown").click().type("".concat(item.peakContactCode));
          cy.get("#inputDropdownDataList > .dropdown > #inputDropdown > .selectInputDropdown").contains("".concat(item.peakContactName)).click(); // ปรเภทภาษี

          cy.get("#inputDropdownPricingType > .dropdown").click();
          cy.get("#inputDropdownPricingType > .dropdown > #inputDropdown").contains("รวมภาษี").click(); // ออกใบกำกับภาษี

          cy.get(".slider");
          item.brachProduct.forEach(function (element, index) {
            // รายการสินค้า
            cy.get("#inputDropdownDataList > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__tags > .multiselect__placeholder").type("".concat(item.brachProduct[index].peakProductCode, "{enter}")); // บีญชี
            // จำนวน

            cy.get(".quantity").eq([index]).type("".concat(item.brachProduct[index].peakProductQuantity, "{enter}"));
            cy.document().then(function (document) {
              var table = document.querySelectorAll('table.product td #inputComponent');
              var price = table[table.length - 2].querySelectorAll("#inputComponent input[id]");
              console.log(price);
              cy.wrap(price[0]).as("item");
              cy.get("@item").type("".concat(item.brachProduct[index].priceRaw, "{enter}"));
            }); // ส่วนลด

            var disCount = item.brachProduct[index].peakProductDiscount / item.brachProduct[index].peakProductQuantity;
            cy.get("#inputDiscount > #inputComponent").eq([index]).type("".concat(disCount, "{enter}"));
            cy.get("#dropdownTax > .dropdown > #inputDropdown").eq([index]).click();
            cy.get("#dropdownTax > .dropdown").eq([index]).contains("7%").click({
              force: true
            });
            cy.get(".buttonAdd").contains("เพิ่มรายการใหม่").click({
              force: true
            });
          });

          if (item.totalShippingFee != null) {
            cy.document().then(function (document) {
              var table = document.querySelectorAll("table.product");
              var account = table[table.length - 1].querySelectorAll("#dropdownAccoutId");
              cy.wrap(account[0]).as("ac");
              cy.get("@ac").type("420109{enter}");
              var price = table[table.length - 1].querySelectorAll("#inputComponent input[id]");
              cy.wrap(price[1]).as("item");
              cy.get("@item").type(item.totalShippingFee);
            });
          }

          if (item.manualDiscount != null) {
            cy.get("#checkbox").click();
            cy.get(".inputDiscount").type("".concat(item.manualDiscount));
          } else if (item.totalDiscount > 0) {
            cy.get("#checkbox").click();
            cy.get(".inputDiscount").type("".concat(item.totalDiscount));
          }

          cy.get("#DropdownPaymentBankAccount").type("เงินสด{enter}");
          cy.wait(500);
          cy.intercept('POST', 'https://peakrealtimeservice-production.azurewebsites.net/api/Receipt/CreateReceiptOnce').as('CreateBill');
          cy.get('#FooterButton > :nth-child(1) > .end > :nth-child(3) > :nth-child(1) > .button > .buttonDefault > div').click({
            force: true
          }); // หัก ณ ที่จ่าย

          cy.wait('@CreateBill');
          cy.get('@CreateBill').then(function (res) {
            var code = res.response.body.responseCode;

            if (code == 200) {
              cy.request('PATCH', "https://memorize-api-teulcdz4qa-as.a.run.app/peak/brachprocess/".concat(data.body[0].id, "/auto-fill"));
            }
          });
        }
      });
    });
  });
});
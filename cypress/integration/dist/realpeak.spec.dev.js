"use strict";

describe('login', function () {
  beforeEach(function () {
    cy.visit('https://secure.peakaccount.com/');
    cy.Username().type('finance@memorizebrownie.com');
    cy.wait(200);
    cy.Password().type('memorize2020');
    cy.wait(600); // cy.get('.input').contains('อีเมล').type('sitthiphong.pro@gmail.com')
    // cy.get('.input').contains('รหัสผ่าน').type('Prompalit1')

    cy.get('button').click();
  });
  context('เลือกกิจการ', function () {
    it('ทดสอบ', function () {
      cy.request('GET', 'https://memorize-api-teulcdz4qa-as.a.run.app/peak/auto').then(function (data) {
        var item = data.body[0];
        cy.wait(1000);
        console.log(item); // cy.get('.lists > :nth-child(2)').click()

        cy.visit('https://secure.peakaccount.com/income/receiptCreate?emi=MTA1NzE5'); // วันที่ออก

        if (item) {
          cy.get('#CalendarSingle').click().type("".concat(item.issuedDate, "{enter}")); // cy.get(':nth-child(3) > #CalendarSingle > [data-v-c1db7b40=""][data-v-2935be6e=""] > .input > input').click().type(`${item.issuedDate}{enter}`)
          // ชื่อลูกค้า

          cy.get('#inputDropdown').click().type("".concat(item.peakContactCode)); // cy.get('[data-v-dfca02f4=""] > #inputDropdownDataList > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__content-wrapper')

          cy.get('#inputDropdownDataList > .dropdown > #inputDropdown').contains("".concat(item.peakContactName)).click(); // cy.get('#inputDropdownDataList > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__content-wrapper').contains(`${item.peakContactName}`).click()
          // ปรเภทภาษี

          cy.get('#inputDropdownPricingType > .dropdown').click();
          cy.get('#inputDropdownPricingType > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__content-wrapper').contains('รวมภาษี').click(); // ออกใบกำกับภาษี

          cy.get('.slider'); // let sum = 0
          // item.brachProduct.forEach(brachProduct => sum += brachProduct.peakProductPrice * brachProduct.peakProductQuantity)
          // console.log(sum);
          // console.log(item.brachProduct);

          item.brachProduct.forEach(function (element, index) {
            // รายการสินค้า
            cy.get('#inputDropdownDataList > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__tags > .multiselect__placeholder').type("".concat(item.brachProduct[index].peakProductCode, "{enter}")); // บีญชี
            // จำนวน

            cy.get('.quantity').eq([index]).type("".concat(item.brachProduct[index].peakProductQuantity, "{enter}")); // ส่วนลด

            var disCount = item.brachProduct[index].peakProductDiscount / item.brachProduct[index].peakProductQuantity;
            cy.get('#inputDiscount > #inputComponent').eq([index]).type("".concat(disCount, "{enter}"));
            cy.get('#dropdownTax > .dropdown > #inputDropdown').eq([index]).click();
            cy.get('#dropdownTax > .dropdown').eq([index]).contains("7%").click({
              force: true
            }); // cy.get('#dropdownTax > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__tags > .multiselect__single > .singleLabel > p').eq([index]).click()
            // cy.get('#dropdownTax > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__content-wrapper > .multiselect__content > :nth-child(3) > .multiselect__option').eq([index]).contains(`7%`).click({force:true})

            cy.get('.buttonAdd > #buttonStyle > .secondary > .blue > button').contains('เพิ่มรายการใหม่').click({
              force: true
            });
            cy.get('#dropdownAccoutId');
          });

          if (item.totalShippingFee != null) {
            cy.document().then(function (document) {
              var table = document.querySelectorAll('table.product');
              var account = table[table.length - 1].querySelectorAll('#dropdownAccoutId');
              cy.wrap(account[0]).as('ac');
              cy.get('@ac').type('420109{enter}');
              var price = table[table.length - 1].querySelectorAll('#inputComponent input[id]');
              cy.wrap(price[1]).as('item');
              cy.get('@item').type(item.totalShippingFee);
            });
          }

          if (item.manualDiscount != null) {
            cy.get('#checkbox').click();
            cy.get('.inputDiscount').type("".concat(item.manualDiscount));
          }

          cy.get('#DropdownPaymentBankAccount').type('เงินสด{enter}'); // cy.get('#DropdownPaymentBankAccount > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__tags').click()
          // cy.get('#DropdownPaymentBankAccount > .dropdown > #inputDropdown > .selectInputDropdown > .multiselect > .multiselect__content-wrapper').contains('เงินสด').click()
          // cy.wait(500)
          // cy.intercept('POST','https://peakrealtimeservice-production.azurewebsites.net/api/Receipt/CreateReceiptOnce').as('CreateBill')
          // cy.get('#FooterButton > :nth-child(1) > .end > :nth-child(3) > :nth-child(1) > .button > .buttonDefault > div').click({force:true})
          // // หัก ณ ที่จ่าย
          // cy.wait('@CreateBill')
          // cy.get('@CreateBill').then((res)=>{
          //     let code = res.response.body.responseCode
          //     if (code == 200) {
          //         cy.request('PATCH',`https://memorize-api-teulcdz4qa-as.a.run.app/peak/brachprocess/${data.body[0].id}/auto-fill`)
          //     }
          // })
          // }
        }
      });
    });
  });
});
"use strict";

var option = {
  method: "POST",
  url: "https://peakrealtimeservice-production.azurewebsites.net/api/Receipt/CreateReceiptOnce",
  headers: {
    Authorization: "Bearer ".concat(token)
  },
  body: {
    merchantId: 105719,
    financeType: 1,
    transaction: {
      actionDate: null,
      id: 0,
      number: number,
      date: item.issuedDate,
      isVat: false,
      discount: item.manualDiscount || 0,
      discountType: 0,
      remark: "",
      status: 4,
      contact: {
        id: "",
        contactCode: item.peakContactCode
      },
      financeTypeId: 1,
      taxStatus: 1,
      isDeleted: false,
      isCreatedTaxInvoice: true,
      referenceName: "",
      referenceId: 0,
      referenceType: 0,
      referencePaymentGroupId: 0,
      transactionSummary: {
        subTotal: 5471.03,
        netAmount: item.totalNetPrice,
        whtTotal: 0,
        vatExemptedTotal: 0,
        zeroVatTotal: 0,
        preVatTotal: 5471.03,
        vatTotal: 382.97,
        subTotalForCalculateWht: 5471.03,
        isAllNoneOrNotSpecifyWht: true,
        isServiceOnly: false,
        sameProductVatType: 3
      }
    }
  }
};